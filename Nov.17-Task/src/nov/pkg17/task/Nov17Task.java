package nov.pkg17.task;

import java.io.File;
import java.util.Scanner;

public class Nov17Task {

    public static void main(String[] args) {
        int one = 0;
        int two = 0;
        int three = 0;
        Scanner sc = null;
        try {
            sc = new Scanner(new File("asmt_3_1_input3.txt")); //asmt_3_1_input1.txt asmt_3_1_input2.txt asmt_3_1_input3.txt
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

        while (sc.hasNextInt()) {
            int val = sc.nextInt();

            if (val == 1) {
                one++;
            } else if (val == 2) {
                two++;
            } else if (val == 3) {
                three++;
            }
        }

        if (one > Math.max(two, three)) {
            System.out.println("One ocurrs the most with " + one);
        } else if (two > Math.max(one, three)) {
            System.out.println("Two occurs the most with " + two);
        } else if (three > Math.max(one, two)) {
            System.out.println("three occurs the most with " + three);
        } else if (one == two) {
            System.out.println("one and two occur the most with " + one);
        } else if (one == three) {
            System.out.println("one and three occur the most with " + one);
        } else if (two == three) {
            System.out.println("two and three occur the most with " + two);
        }

    }
}
